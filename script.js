"use strict";

(function () {
  initSelects();
  initCardNumber();
  initCardOwner();
  initDateSelect();
  initCvvInput();

  function initCardNumber() {
    const cardNumberInput = document.getElementById("cardNumberInput");
    const cardNumberDisplay = document.getElementById("cardNumber");
    const cardNumberMask = IMask(cardNumberInput, {
      mask: "0000-0000-0000-0000",
      lazy: false,
      placeholderChar: "#",
    });
    cardNumberDisplay.innerHTML = cardNumberMask.value;
    cardNumberInput.addEventListener("input", () => {
      cardNumberDisplay.innerHTML = cardNumberMask.value;
    });
  }

  function initCardOwner() {
    const cardOwnerInput = document.getElementById("cardOwnerInput");
    const cardOwnerDisplay = document.getElementById("cardOwner");
    const cardOwnerMask = IMask(cardOwnerInput, {
      mask: '******[********************]',
      lazy: false,
      placeholderChar: "#",
      prepare: (str) => str.toUpperCase(),
    });
    cardOwnerDisplay.innerHTML = cardOwnerMask.value;
    cardOwnerInput.addEventListener("input", () => {
      cardOwnerDisplay.innerHTML = cardOwnerMask.value;
    });
  }

  function initDateSelect() {
    const monthsSelect = document.getElementById("months");
    const monthDisplay = document.getElementById("monthValue");
    const yearsSelect = document.getElementById("years");
    const yearsDisplay = document.getElementById("yearsValue");

    monthsSelect.addEventListener("change", (event) => {
      const value = event.target.value;
      monthDisplay.innerText = value ? value : 'MM';
    });

    yearsSelect.addEventListener("change", (event) => {
      const value = event.target.value;
      yearsDisplay.innerText = value ? value : 'YY';
    });
  }

  function initCvvInput() {
    const cvvInput = document.getElementById("cardCvvInput");
    const cvvDisplay = document.getElementById("cardCvv");
    const cvvInputMask = IMask(cvvInput, {
      mask: '000',
      lazy: false,
      placeholderChar: "#",
    });
    cvvDisplay.innerHTML = cvvInputMask.value;
    cvvInput.addEventListener("input", () => {
      cvvDisplay.innerHTML = cvvInputMask.value;
    });

    function addFlipCardListeners(inputElement) {
      inputElement.addEventListener("focusin", flipCard);
      inputElement.addEventListener("focusout", flipCard);
    }

    function flipCard() {
      const cardFront = document.querySelector(".card--front");
      const cardBack = document.querySelector(".card--back");
      const rotationClassName = "card--rotated";

      if (cardFront.classList.contains(rotationClassName)) {
        cardFront.classList.remove(rotationClassName);
        cardBack.classList.add(rotationClassName);
      } else {
        cardFront.classList.add(rotationClassName);
        cardBack.classList.remove(rotationClassName);
      }
    }

    addFlipCardListeners(cvvInput);
  }

  function initSelects() {
    function addYearsSelectOptions() {
      const years = document.getElementById("years");
      years.innerHTML += "<option></option>";
      for (let i = 0; i < 9; i++) {
        const year = new Date().getFullYear() + i;
        const yearValue = year - 2000;
        years.innerHTML +=
          '<option value="' + yearValue + '">' + year + "</option>";
      }
    }

    function addMonthsSelectOptions() {
      const months = document.getElementById("months");
      months.innerHTML += "<option></option>";
      for (let i = 1; i < 13; i++) {
        const valueWithLeadingZero = i < 10 ? "0" + i : i;
        months.innerHTML +=
          '<option value="' + valueWithLeadingZero + '">' + i + "</option>";
      }
    }

    addYearsSelectOptions();
    addMonthsSelectOptions();
  }
})();
